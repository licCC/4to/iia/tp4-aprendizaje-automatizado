\documentclass{article}
\usepackage[utf8]{inputenc} %codificacion de caracteres que permite tildes
\usepackage[spanish]{babel}

\usepackage{float}
\usepackage{graphicx}
\usepackage{subcaption}
\graphicspath{ {./Resources/} }
\usepackage{csvsimple}
\renewcommand{\spanishtablename}{Tabla}
\usepackage{listings}

%matrices
\usepackage{amsmath} %para poder definir la funcion por partes
\usepackage{blkarray} %permite ponerle etiquetas a las columnas y filas de las matrices

\usepackage[
top    = 2cm,
bottom = 1.5cm,
left   = 1.5cm,
right  = 1.5cm]
{geometry}

%opening
\title{TP4 - Introducción a la inteligencia artificial}
\author{Delfina Martín - Sebastian Morales}
\date{Junio 2021}

\begin{document}

	\maketitle
	\pagebreak

	\section{Caracteristicas del dataset utilizado}

	El conjunto de datos utilizado tiene la siguiente forma

	\begin{table}[H]
		\centering
		\csvautotabular{Resources/headDataset.csv}
		\caption{Primeros datos del dataset yeast}
	\end{table}

	donde los 8 últimos atributos son predictivos y el primero identifica de manera única a cada yeast. Los atributos son:

	\begin{itemize}
		\item \textbf{Sequence name} identificador único
		\item \textbf{mcg} método de McGeoch para reconocer secuencias
		\item \textbf{gvh} método de Von Heijne para reconocer secuencias
		\item \textbf{alm} puntaje de la membrana ALOM
		\item \textbf{mit} puntaje del análisis que discrimina el contenido del aminoácido de la región N-terminal de la mitocondria y las proteínas no mitocondriales
		\item \textbf{erl} presencia de la subcadena HDEL. Atributo binario (0.5 / 1)
		\item \textbf{pox} señal de reonocimiento peroxisomal
		\item \textbf{vac} puntaje del análisis que discrimina el contenido de aminoácido de proteínas vacuolar y extracelulares
		\item \textbf{nuc} puntaje del análisis que discrimina las señales de localización nuclear de proteínas nucleares y no nucleares.
	\end{itemize}

	Y las clases son:

	\begin{center}
		\begin{tabular}{r|l}
			CYT&(cytosolic or cytoskeletal)\\
			NUC&(nuclear)\\
			MIT&(mitochondrial)\\
			ME3&(membrane protein, no N-terminal signal)\\
			ME2&(membrane protein, uncleaved signal)\\
			ME1&(membrane protein, cleaved signal)\\
			EXC&(extracellular)\\
			VAC&(vacuolar)\\
			POX&(peroxisomal)\\
			ERL&(endoplasmic reticulum lumen)
		\end{tabular}
	\end{center}

	El dataset no contiene entradas no clasificadas y la distribución de las clases es:

	\begin{table}[H]
		\centering
		\csvautotabular{Resources/classDist.csv}
		\caption{Distribución de las clases}
		\label{tab:classDist}
	\end{table}

	con un total de 1484 filas.

	\subsection{Visualización de la dispersión de los datos}

	Previo al entrenamiento del modelo generamos algunas gráficas de dispersión (\textit{scatter plots}) para hacernos una mejor idea de la (in)dependencia de los atributos. Algunos resultados no aportaron demasiado al análisis del modelo generado (\ref{fig:vacnuc}) mientras que otros fueron altamente esclarecedores (\ref{fig:almmit}, \ref{fig:mcgalm}).

	\begin{figure}[H]
		\centering
		\includegraphics[scale=0.7]{fail.pdf}
		\caption{Dispersión de vac en función de nuc}
		\label{fig:vacnuc}
	\end{figure}

	\begin{figure}[H]
		\centering
		\begin{subfigure}[b]{0.4\textwidth}
			\centering
			\includegraphics[scale=0.4]{success1.pdf}
			\caption{Dispersión de alm en función de mit}
			\label{fig:almmit}
		\end{subfigure}
		\qquad
		\begin{subfigure}[b]{0.4\textwidth}
			\centering
			\includegraphics[scale=0.4]{success2.pdf}
			\caption{Dispersión de mcg en función de alm}
			\label{fig:mcgalm}
		\end{subfigure}
		\caption{Gráficas de dispersión que contribuyen al entendimiento del modelo generado}
	\end{figure}

	\paragraph{Nota} todas las gráficas están adjuntas para una mejor visualización. Se incluyen en el informe únicamente para que resulte más fácil hacer referencia a ellas.

	\subsection{Separación de datos de entrenamiento y datos de test}

	Del total del conjunto de datos el 80\% se utiliza para entrenamiento y el 20\% restante para testing de la siguiente forma: se divide al conjunto en 5 partes disjuntas de forma aleatoria y se toma una de esas partes como conjunto de testing y las 4 restantes como conjunto de entrenamiento.

	\begin{lstlisting}[language=R]
	# Generamos 5 folds
	indexdata <- createFolds(t(data[, "Clase"]), k = 5)

	# Separamos el conjunto de test del de trainning (80%/20%)
	yeasttest <- data[indexdata[[4]], ]
	yeasttrain <- data[setdiff(seq(1:dim(data)[1]), indexdata[[4]]), ]
	\end{lstlisting}

	\section{Entrenamiento del modelo}

	Se generaron 2 ajustes sobre los datos: el primero a partir de la medida de selección de atributo (\textit{ASM, attribute selection measure}) ganancia de información y el segundo a partir del índice de Gini. Los resultados obtenidos se pueden ver en (\ref{fig:treeinf}) y (\ref{fig:treegini}). Para entrenar ambos modelos utilizamos todos los atributos disponibles.

	Para generar los modelos usamos los parámetros por defecto:
	\begin{itemize}
		\item minsplit = 20 que es la mínima cantidad de observaciones que deben existir en un nodo para que se pueda dividir
		\item minbucket = round(minsplit/3) que es el mínimo numero de observaciones en cada hoja
		\item maxdepth = 30 que es la máxima profundidad del arbol generado en donde la raiz tiene profundidad 0
		\item cp = 0.01 que es el parámetro de complejidad. Las divisiones que no aumentan el grado de ajuste con respecto al conjunto de entrenamiento por un factor de cp no se realizan. Muy probablemente, de hacerse estas divisiones, serían removidas en una etapa posterior de validación cruzada.
	\end{itemize}

	\begin{figure}[H]
		\centering
		\includegraphics[scale=0.7]{decisionTreeInformation.pdf}
		\caption{Árbol de decisión generado a partir de la ganancia de información}
		\label{fig:treeinf}
	\end{figure}

	Uno de los resultados arrojados por la función summary sobre el árbol generado es

	\begin{table}[H]
		\centering
		\begin{tabular}{|c|c|c|c|c|c|}
			\hline
			\multicolumn{6}{|c|}{Variable importance}\\
			\hline
			alm&mcg&mit&gvh&nuc&vac\\
			\hline
			38&27&14&12&7&1\\
			\hline
		\end{tabular}
	\end{table}

	con lo cual se elige el atributo alm como raíz del árbol.

	\begin{figure}[H]
		\centering
		\includegraphics[scale=0.7]{decisionTreeGini.pdf}
		\caption{Árbol de decisión generado a partir del índice de Gini}
		\label{fig:treegini}
	\end{figure}

	Uno de los resultados arrojados por la función summary sobre el árbol generado es

	\begin{table}[H]
		\centering
		\begin{tabular}{|c|c|c|c|c|c|}
			\hline
			\multicolumn{6}{|c|}{Variable importance}\\
			\hline
			alm&mcg&gvh&mit&nuc&vac\\
			\hline
			29&22&19&17&12&2\\
			\hline
		\end{tabular}
	\end{table}

	con lo cual se elige el atributo alm como raíz del árbol.

	\pagebreak
	\section{Etapa de predicción de los modelos mediante las métricas de desempeño}

	\subsection{Modelo ganancia de información}

	La matriz de confusión para el modelo de ganancia de información es la siguiente

	\begin{align}
	\begin{blockarray}{ccccccccccc}
		&CYT&ERL&EXC&ME1&ME2&ME3&MIT&NUC&POX&VAC\\
		\begin{block}{r[cccccccccc]}
		CYT&51&0&1&0&3&0&17&22&2&2\\
		ERL&0&0&0&0&0&0&0&0&0&0\\
		EXC&3&0&3&1&3&0&0&2&1&0\\
		ME1&0&0&1&8&3&0&1&0&0&0\\
		ME2&0&0&0&0&1&1&0&0&0&0\\
		ME3&1&0&0&0&0&32&2&6&0&2\\
		MIT&8&1&2&0&0&0&26&8&1&1\\
		NUC&30&0&0&0&0&0&3&48&0&1\\
		POX&0&0&0&0&0&0&0&0&0&0\\
		VAC&0&0&0&0&0&0&0&0&0&0\\
		\end{block}\nonumber
	\end{blockarray}
	\end{align}

	y las métricas para este modelo son

	\begin{table}[H]
		\centering
		\begin{tabular}{|c|c|c|}
			\hline
			Accuracy&Recall&Specifity\\
			\hline
			0.5671&0.4024&0.9429\\
			\hline
		\end{tabular}
	\end{table}

	donde el recall y la especificidad se calcularon a partir de los recalls y especificidades individuales por clase como el promedio. La accuracy por su parte se calculó con una tasa de información de (0.5087, 0.6241).

	\subsection{Modelo índice de Gini}

	La matriz de confusión para el modelo de índice de Gini es la siguiente

	\begin{align}
	\begin{blockarray}{ccccccccccc}
	&CYT&ERL&EXC&ME1&ME2&ME3&MIT&NUC&POX&VAC\\
	\begin{block}{r[cccccccccc]}
		CYT&63&0&1&0&3&0&17&28&2&3\\
		ERL&0&0&0&0&0&0&0&0&0&0\\
		EXC&2&0&3&1&2&0&0&2&1&0\\
		ME1&0&0&1&8&3&0&1&0&0&0\\
		ME2&0&0&0&0&1&1&0&0&0&0\\
		ME3&1&0&0&0&0&32&2&6&0&2\\
		MIT&8&1&2&0&0&0&26&8&1&1\\
		NUC&19&0&0&0&1&0&3&42&0&0\\
		POX&0&0&0&0&0&0&0&0&0&0\\
		VAC&0&0&0&0&0&0&0&0&0&0\\
	\end{block}\nonumber
	\end{blockarray}
	\end{align}

	y las métricas para este modelo son

	\begin{table}[H]
		\centering
		\begin{tabular}{|c|c|c|}
			\hline
			Accuracy&Recall&Specifity\\
			\hline
			0.5872&0.3079&0.5781\\
			\hline
		\end{tabular}
	\end{table}

	donde el recall y la especificidad se calcularon a partir de los recalls y especificidades individuales por clase como el promedio. La accuracy por su parte se calculó con una tasa de información de (0.5087, 0.6241).

	\section{Procesamiento post generado del modelo - podado}

	En el script R adjunto se puede ver que se hicieron pruebas de podado con parámetro cp = 0.05 pero solamente para verificar que la accuracy resultante, 0.5402685, es menor a las obtenidas en la sección anterior. Claramente los modelos producto del podado son más pobres que los que se pueden visualizar en la sección de entrenamiento.

	\section{Conclusiones}

	Teniendo en cuenta lo desbalanceado que está el dataset (\ref{tab:classDist}) y el hecho de que la mayoría de las métricas las calculamos como el promedio de los valores individuales, tiene sentido que los resultados no sean sobresalientes. Tanto para el modelo que utiliza ganancia de información como para el modelo que utiliza el índice de Gini los resultados no son aceptables y probablemente sea necesario hacer un preprocesamiento de los datos para poder sortear algunos de los problemas que presenta este conjunto de datos.

	\subsubsection*{El promedio tiene pérdida de información}
	Al calcular las métricas a partir del promedio, la pérdida de información es alta si los datos tienen una alta variabilidad por lo cual puede ser conveniente combinarlo con otras medidas como la mediana y la desviación estándar.

	\textbf{Nota}: no calculamos la precisión generalizada ya que para algunas clases este valor era NaN producto de una división por cero.

	\subsubsection*{Comparación propiamente dicha}

	\begin{itemize}
		\item En el modelo que utiliza ganancia de información, la máxima precisión se consiguió para la clase ME3 con un valor de 0.744186. Con respecto al recall, el máximo obtenido corresponde a la clase ME3 con un valor de 0.969697. Mientras que el mínimo es 0 y está asociado a las clases POX y VAC.
		\item En el modelo que utiliza índice de Gini, la máxima precisión se consiguió para la clase ME3 con un valor de 0.7441. Con respecto al recall, el máximo obtenido corresponde a la clase ME1 con un valor de 0.8889. Mientras que el mínimo es 0 y está asociado a la clase ERL, POX y VAC.
	\end{itemize}

	\begin{table}[H]
		\centering
		\begin{tabular}{cccc}
			&Accuracy&Recall&Specifity\\
			\hline
			Ganancia de información&0.5671&0.4024&0.9429\\
			\hline
			Índice de Gini&0.5872&0.3079&0.5781\\
			\hline
		\end{tabular}
		\caption{Vista comparativa de métricas}
	\end{table}

	En este caso, como las métricas son relativamente similares, elegimos la que tiene máxima accuracy (índice de Gini) ya que no nos interesa priorizar la correcta clasificación de ninguna clase en particular.

	\subsubsection*{Conjunto de validación}
	Si además contaramos con un conjunto independiente de validación, podríamos tener en cuenta el menor error sobre este conjunto y así también elegir el árbol de decisión que menos sobreajuste tenga, o en otras palabras, el que más generalice y menos memorice.

	\subsubsection*{Podado}
	Como se mencionó en la sección 4, el accuracy del árbol producto de un podado, tiene menor accuracy que el árbol sin podar. Si la complejidad del árbol degenera en un sobreajuste, el modelo se podría beneficiar de un podado ya que se eliminarían las bifurcaciones de los últimos niveles.

	\vspace{5mm}
	Como comentario final, el hecho de estar trabajando con un dominio muy alejado del área en donde nos desenvolvemos, actuó en detrimento del análisis del modelo. De haber sido un dominio más cercano a nosotrxs, hubieramos podido generar hipótesis de antemano respecto a la dependencia o no dependencia de los atributos. Lo cual a su vez pudo haber contribuido al desarrollo del trabajo.

\end{document}